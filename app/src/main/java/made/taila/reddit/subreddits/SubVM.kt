package made.taila.reddit.subreddits

import made.taila.reddit.NumberFormatter
import made.taila.reddit.model.SubReddit

class SubVM(val subReddit: SubReddit) {

    fun name() = "r/${subReddit.name}"
    fun numSubscribers() = "${NumberFormatter.format(subReddit.subscribers.toLong())} subscribers"
    fun iconImageUrl() = subReddit.iconImageUrl

}