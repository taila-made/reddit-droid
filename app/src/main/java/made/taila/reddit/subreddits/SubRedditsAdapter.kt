package made.taila.reddit.subreddits

import android.arch.lifecycle.Observer
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import made.taila.reddit.MainVM
import made.taila.reddit.R
import made.taila.reddit.common.Event
import made.taila.reddit.databinding.ItemSubBinding
import made.taila.reddit.feed.SubRedditsVM
import made.taila.reddit.model.SubReddit

class SubRedditsAdapter(private val subRedditsVM: SubRedditsVM, private val mainVM: MainVM, fragment: SubRedditsFragment) :
        RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    init {
        subRedditsVM.subRedditsSearchResponse.observe(fragment, Observer {
            notifyDataSetChanged()
        })
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = DataBindingUtil.inflate<ItemSubBinding>(
                LayoutInflater.from(parent.context),
                R.layout.item_sub,
                parent,
                false
        )
        return SubVH(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        holder as SubVH
        holder.bind(getSubRedditAtPosition(position))
    }

    private fun getSubRedditAtPosition(position: Int): SubReddit {
        return subRedditsVM.subRedditsSearchResponse.value!!.subReddits[position]
    }

    override fun getItemCount(): Int {
        return try {
            subRedditsVM.subRedditsSearchResponse.value!!.subReddits.size
        } catch (e: NullPointerException) {
            0
        }
    }

    inner class SubVH(val itemSubBinding: ItemSubBinding) : RecyclerView.ViewHolder(itemSubBinding.root) {
        fun bind(subReddit: SubReddit) {
            itemSubBinding.subVM = SubVM(subReddit)
            itemSubBinding.notifyChange()
            itemView.setOnClickListener {
                when (it.id) {
                    itemSubBinding.btnSubscribe.id -> {
                        // TODO
                    }
                    else -> {
                        mainVM.subRedditSelected.value = Event(subReddit)
                    }
                }
            }
        }
    }
}