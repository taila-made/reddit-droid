package made.taila.reddit.subreddits

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import made.taila.reddit.MainVM
import made.taila.reddit.R
import made.taila.reddit.databinding.FragmentSubRedditsBinding
import made.taila.reddit.feed.SubRedditsVM

class SubRedditsFragment : Fragment() {

    lateinit var subRedditSearchVM: SubRedditsVM
    lateinit var mainVM: MainVM
    lateinit var subRedditsBinding: FragmentSubRedditsBinding

    companion object {
        @JvmStatic
        fun newInstance(): SubRedditsFragment {
            return SubRedditsFragment()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        subRedditSearchVM = ViewModelProviders.of(this)[SubRedditsVM::class.java]
        mainVM = ViewModelProviders.of(activity!!)[MainVM::class.java]
        subRedditSearchVM.searchForSubReddits(mainVM.searchTerm.value!!)
        observe()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        subRedditsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_sub_reddits, container, false)
        setupRecyclerView()
        return subRedditsBinding.root
    }

    private fun setupRecyclerView() {
        subRedditsBinding.feedRecyclerView.adapter = SubRedditsAdapter(subRedditSearchVM, mainVM, this)
        subRedditsBinding.feedRecyclerView.layoutManager = LinearLayoutManager(context)
    }

    private fun observe() {
        mainVM.searchTerm.observe(this, Observer {
            subRedditSearchVM.searchForSubReddits(it!!)
        })
    }

}