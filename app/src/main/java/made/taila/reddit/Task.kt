package made.taila.reddit

sealed class Task {
    object InProgress : Task()
    data class Error(val throwable: Throwable) : Task()
    data class CompletedSuccessfully(val value: Any) : Task()
}