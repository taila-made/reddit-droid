package made.taila.reddit.ui

import android.databinding.BindingAdapter
import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

@BindingAdapter("app:layout_width")
fun View.setLayoutWidth(width: Int) {
    val lp = layoutParams
    lp.width = width
    layoutParams = lp
}

@BindingAdapter("app:layout_height")
fun View.setLayoutHeight(height: Int) {
    val lp = layoutParams
    lp.height = height
    layoutParams = lp
}

@BindingAdapter("app:glideImageUrl")
fun ImageView.glideImageUrl(url: String) {
    Glide.with(context).load(url).into(this)
}

@BindingAdapter("app:glideImageUrl", "app:placeHolder", "app:circle")
fun ImageView.glideImageUrl(url: String, placeholderResId: Int, circle: Boolean = false) {
    Glide.with(context).load(url)
        .apply(RequestOptions.placeholderOf(placeholderResId))
        .apply {
            if (circle) apply(RequestOptions.circleCropTransform())
        }
        .into(this)
}