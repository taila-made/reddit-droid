package made.taila.reddit.ui

import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ValueAnimator
import android.content.res.ColorStateList
import android.support.v4.widget.ImageViewCompat
import android.view.View
import android.widget.Button
import android.widget.ImageView

fun View.popAnimation(popMax: Float = 1.2f): ValueAnimator {
    return ValueAnimator.ofFloat(1f, popMax, 1f).apply {
        addUpdateListener {
            scaleX = animatedValue as Float
            scaleY = animatedValue as Float
        }
    }
}

fun View.elevationAnimator(to: FloatArray): ValueAnimator {
    return ValueAnimator.ofFloat(*to).apply {
        addUpdateListener {
            elevation = animatedValue as Float
        }
    }
}

fun ValueAnimator.ofArgbCompat(to: IntArray): ValueAnimator {
    return ValueAnimator().apply {
        setIntValues(*to)
        setEvaluator(ArgbEvaluator.newInstance())
    }
}

fun ImageView.tintAnimator(to: IntArray): ValueAnimator {
    return ValueAnimator().ofArgbCompat(to).apply {
        addUpdateListener {
            ImageViewCompat.setImageTintList(this@tintAnimator, ColorStateList.valueOf(animatedValue as Int))
        }
    }
}

fun Button.bgColorAnimator(toColors: IntArray): ValueAnimator {
    return ValueAnimator.ofArgb(*toColors).apply {
        addUpdateListener {
            backgroundTintList = ColorStateList.valueOf(animatedValue as Int)
        }
    }
}

fun View.scaleXAnimator(to: FloatArray): ValueAnimator {
    return ValueAnimator.ofFloat(*to).apply {
        addUpdateListener {
            scaleX = animatedValue as Float
        }
    }
}

fun View.scaleYAnimator(to: FloatArray): ValueAnimator {
    return ValueAnimator.ofFloat(*to).apply {
        addUpdateListener {
            scaleY = animatedValue as Float
        }
    }
}

fun Button.onSelectAnimator(toColor: Int, popMax: Float = 1.1f): Animator {
    val bgColorAnimator = bgColorAnimator(intArrayOf(toColor))
    val scaleAnimator = popAnimation(popMax)
    return AnimatorSet().apply {
        playTogether(bgColorAnimator, scaleAnimator)
    }
}