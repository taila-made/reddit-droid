package made.taila.reddit.ui

import android.app.Activity
import android.content.Context
import android.content.res.Resources
import android.support.constraint.Group
import android.view.View
import android.view.inputmethod.InputMethodManager

fun Activity.hideKeyboard() {
    val view = currentFocus
    if (view != null) {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }
}

fun getScreenWidth(): Int {
    return Resources.getSystem().displayMetrics.widthPixels
}

fun getScreenHeight(): Int {
    return Resources.getSystem().displayMetrics.heightPixels
}

fun View.findViewsByIds(viewIds: IntArray): Array<View> {
    return Array<View>(viewIds.size) { i -> findViewById(viewIds[i]) }
}

fun Group.onClick(onClickListener: View.OnClickListener) {
    referencedIds.forEach {
        (parent as View).findViewById<View>(it).setOnClickListener(onClickListener)
    }
}