package made.taila.reddit

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import made.taila.reddit.common.Event
import made.taila.reddit.model.Post
import made.taila.reddit.model.SubReddit

class MainVM : ViewModel() {

    val searchTerm = MutableLiveData<String>()
    val subRedditSelected = MutableLiveData<Event<SubReddit>>()
    val postSelected = MutableLiveData<Event<Post>>()

}