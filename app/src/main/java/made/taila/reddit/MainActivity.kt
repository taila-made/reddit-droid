package made.taila.reddit

import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.KeyEvent
import dagger.android.AndroidInjection
import made.taila.reddit.common.EventObserver
import made.taila.reddit.databinding.ActivityMainBinding
import made.taila.reddit.feed.FeedFragment
import made.taila.reddit.model.SubReddit
import made.taila.reddit.post.PostFragment
import made.taila.reddit.subreddits.SubRedditsFragment
import made.taila.reddit.ui.hideKeyboard

class MainActivity : AppCompatActivity() {

    companion object {
        const val FRAGMENT_TAG_ROOT = "Root"
    }

    private lateinit var binding: ActivityMainBinding
    lateinit var mainVM: MainVM

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        mainVM = ViewModelProviders.of(this)[MainVM::class.java]
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        if (supportFragmentManager.backStackEntryCount == 0) {
            supportFragmentManager.beginTransaction()
                    .replace(binding.fragmentHolder.id, FeedFragment.newInstance(null), FRAGMENT_TAG_ROOT)
                    .commit()
        }

        observe()
        listenForSearch()
    }

    fun listenForSearch() {
        binding.searchBar.setOnKeyListener { view, i, keyEvent ->
            if (keyEvent.action == KeyEvent.ACTION_DOWN && i == KeyEvent.KEYCODE_ENTER) {
                if (!binding.searchBar.text.isNullOrEmpty()) {
                    mainVM.searchTerm.value = binding.searchBar.text.toString()
                    showSearchFragment()
                }
                hideKeyboard()
                binding.searchBar.clearFocus()
                true
            } else false
        }
    }

    private fun showFeedFragment(subReddit: SubReddit? = null) {
        supportFragmentManager.beginTransaction()
                .addToBackStack(null)
                .replace(binding.fragmentHolder.id, FeedFragment.newInstance(subReddit), FRAGMENT_TAG_ROOT)
                .commit()
    }

    private fun showSearchFragment() {
        supportFragmentManager.beginTransaction()
                .addToBackStack(null)
                .replace(binding.fragmentHolder.id, SubRedditsFragment.newInstance())
                .commit()
    }

    private fun showPostFragment(postUrl: String) {
        supportFragmentManager.beginTransaction()
                .addToBackStack(null)
                .replace(binding.fragmentHolder.id, PostFragment.newInstance(postUrl))
                .commit()
    }

    private fun observe() {
        mainVM.subRedditSelected.observe(this, EventObserver {
            showFeedFragment(it)
        })
        mainVM.postSelected.observe(this, EventObserver {
            showPostFragment(it.url)
        })
    }
}
