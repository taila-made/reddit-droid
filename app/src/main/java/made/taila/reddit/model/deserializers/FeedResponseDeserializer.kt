package made.taila.reddit.model.deserializers

import android.util.Log
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import com.fasterxml.jackson.databind.module.SimpleModule
import made.taila.reddit.model.FeedResponse
import made.taila.reddit.model.Listing
import made.taila.reddit.model.Post

class FeedResponseDeserializer(clazz: Class<FeedResponse>? = null) : StdDeserializer<FeedResponse>(clazz) {

    override fun deserialize(parser: JsonParser, ctx: DeserializationContext): FeedResponse {
        val response = FeedResponse()
        try {
            val listing = Listing()
            val node: JsonNode = parser.codec.readTree(parser)
            listing.kind = node.get("kind").asText()
            val dataNode = node.get("data")
            listing.dist = dataNode.get("dist").asInt()
            listing.after = dataNode.get("after").asText()
            listing.before = dataNode.get("before").asText()

            response.posts = mutableListOf()
            val module = SimpleModule()
            module.addDeserializer(Post::class.java, PostDeserializer())
            val objectMapper = ObjectMapper().apply {
                registerModule(module)
            }
            dataNode.get("children").asIterable().forEach {
                response.posts.add(objectMapper.readValue(it.toString(), Post::class.java))
            }
        } catch (e: Exception) {
            Log.e("FeedResponseDeserializer", e.localizedMessage, e)
        } catch (e: Exception) {
            Log.e("FeedResponseDeserializer", e.localizedMessage, e)
        }
        return response
    }
}