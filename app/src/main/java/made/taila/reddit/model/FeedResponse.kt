package made.taila.reddit.model

import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import made.taila.reddit.model.deserializers.FeedResponseDeserializer

@JsonDeserialize(using = FeedResponseDeserializer::class)
data class FeedResponse(
    var listing: Listing? = null,
    var posts: MutableList<Post> = mutableListOf()
)