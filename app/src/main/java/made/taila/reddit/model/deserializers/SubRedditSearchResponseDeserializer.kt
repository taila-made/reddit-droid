package made.taila.reddit.model.deserializers

import android.util.Log
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import com.fasterxml.jackson.databind.module.SimpleModule
import made.taila.reddit.model.Listing
import made.taila.reddit.model.SubReddit
import made.taila.reddit.model.SubRedditSearchResponse

class SubRedditSearchResponseDeserializer(clazz: Class<SubRedditSearchResponse>? = null) : StdDeserializer<SubRedditSearchResponse>(clazz) {

    override fun deserialize(parser: JsonParser, ctx: DeserializationContext): SubRedditSearchResponse {
        val response = SubRedditSearchResponse()
        try {
            val listing = Listing()
            val node: JsonNode = parser.codec.readTree(parser)
            listing.kind = node.get("kind").asText()
            val dataNode = node.get("data")
            listing.dist = dataNode.get("dist").asInt()
            listing.after = dataNode.get("after").asText()
            listing.before = dataNode.get("before").asText()

            response.subReddits = mutableListOf()
            val module = SimpleModule()
            module.addDeserializer(SubReddit::class.java, SubRedditDeserializer())
            val objectMapper = ObjectMapper().apply {
                registerModule(module)
            }
            dataNode.get("children").asIterable().forEach {
                response.subReddits.add(objectMapper.readValue(it.toString(), SubReddit::class.java))
            }
        } catch (e: Exception) {
            Log.e("SubRedditSearchResponseDeserializer", e.localizedMessage, e)
        } catch (e: Exception) {
            Log.e("SubRedditSearchResponseDeserializer", e.localizedMessage, e)
        }
        return response
    }
}