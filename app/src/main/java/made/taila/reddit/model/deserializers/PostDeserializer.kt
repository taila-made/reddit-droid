package made.taila.reddit.model.deserializers

import android.util.Log
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import made.taila.reddit.model.Post

class PostDeserializer(clazz: Class<Post>? = null) : StdDeserializer<Post>(clazz) {
    override fun deserialize(parser: JsonParser, ctx: DeserializationContext): Post {
        val post = Post()
        try {
            val node: JsonNode = parser.codec.readTree(parser)
            post.kind = node.get("kind").asText()
            val postData = node.get("data")
            if (postData.hasNonNull("subreddit")) {
                post.subreddit = postData.get("subreddit").asText()
            }
            if (postData.hasNonNull("author")) {
                post.author = postData.get("author").asText()
            }
            if (postData.hasNonNull("url")) {
                post.url = postData.get("url").asText()
            }
            if (postData.hasNonNull("title")) {
                post.title = postData.get("title").asText()
            }
            if (postData.hasNonNull("score")) {
                post.score = postData.get("score").asInt()
            }
            if (postData.hasNonNull("num_comments")) {
                post.numComments = postData.get("num_comments").asInt()
            }
            if (postData.hasNonNull("thumbnail")) {
                post.thumbnail = postData.get("thumbnail").asText()
            }
            if (postData.hasNonNull("preview")) {
                post.preview = postData.get("preview").get("images").asIterable().first().get("source").get("url").asText()
                post.previewWidth = postData.get("preview").get("images").asIterable().first().get("source").get("width").asInt()
                post.previewHeight = postData.get("preview").get("images").asIterable().first().get("source").get("height").asInt()
            }
        } catch (e: Exception) {
            Log.e("PostDeserializer", e.localizedMessage, e)
        } catch (e: Exception) {
            Log.e("PostDeserializer", e.localizedMessage, e)
        }
        return post
    }
}