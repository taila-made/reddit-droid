package made.taila.reddit.model.deserializers

import android.util.Log
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import made.taila.reddit.model.SubReddit

class SubRedditDeserializer(clazz: Class<SubReddit>? = null) : StdDeserializer<SubReddit>(clazz) {
    override fun deserialize(parser: JsonParser, ctx: DeserializationContext): SubReddit {
        val subReddit = SubReddit()
        try {
            val node: JsonNode = parser.codec.readTree(parser)
            subReddit.kind = node.get("kind").asText()
            val subData = node.get("data")
            subReddit.name = subData.get("display_name").asText()
            if (subData.hasNonNull("icon_img")) {
                subReddit.iconImageUrl = subData.get("icon_img").asText()
            }
            if (subData.hasNonNull("public_description")) {
                subReddit.publicDescription = subData.get("public_description").asText()
            }
            if (subData.hasNonNull("subscribers")) {
                subReddit.subscribers = subData.get("subscribers").asInt()
            }
        } catch (e: Exception) {
            Log.e("SubRedditDeserializer", e.localizedMessage, e)
        } catch (e: Exception) {
            Log.e("SubRedditDeserializer", e.localizedMessage, e)
        }
        return subReddit
    }
}