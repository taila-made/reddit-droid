package made.taila.reddit.model

import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import made.taila.reddit.model.deserializers.SubRedditSearchResponseDeserializer

@JsonDeserialize(using = SubRedditSearchResponseDeserializer::class)
data class SubRedditSearchResponse(
    var listing: Listing? = null,
    var subReddits: MutableList<SubReddit> = mutableListOf()
)