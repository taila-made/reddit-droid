package made.taila.reddit.model

import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import made.taila.reddit.model.deserializers.PostDeserializer

@JsonDeserialize(using = PostDeserializer::class)
data class Post(
    var kind: String = "",
    var subreddit: String = "",
    var url: String = "",
    var title: String = "",
    var author: String = "",
    var score: Int = 0,
    var numComments: Int = 0,
    var thumbnail: String = "",
    var preview: String = "",
    var previewWidth: Int = 0,
    var previewHeight: Int = 0
)