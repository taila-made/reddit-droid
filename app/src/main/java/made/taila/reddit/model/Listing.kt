package made.taila.reddit.model

import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import made.taila.reddit.model.deserializers.ListingDeserializer

@JsonDeserialize(using = ListingDeserializer::class)
class Listing(
    var kind: String = "",
    var before: String? = null,
    var after: String? = null,
    var dist: Int = -1
)