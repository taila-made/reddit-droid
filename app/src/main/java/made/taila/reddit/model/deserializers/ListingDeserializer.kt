package made.taila.reddit.model.deserializers

import android.util.Log
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import made.taila.reddit.model.Listing

class ListingDeserializer(clazz: Class<Listing>? = null) : StdDeserializer<Listing>(clazz) {

    override fun deserialize(parser: JsonParser, ctx: DeserializationContext): Listing {
        val listing = Listing()
        try {
            val node: JsonNode = parser.codec.readTree(parser)
            listing.kind = node.get("kind").asText()
            listing.dist = node.get("dist").asInt()
            listing.after = node.get("after").asText()
            listing.before = node.get("before").asText()
        } catch (e: Exception) {
            Log.e("ListingDeserializer", e.localizedMessage, e)
        } catch (e: Exception) {
            Log.e("ListingDeserializer", e.localizedMessage, e)
        }
        return listing
    }
}