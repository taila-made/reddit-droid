package made.taila.reddit.model

import android.os.Parcel
import android.os.Parcelable
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import made.taila.reddit.model.deserializers.SubRedditDeserializer

@JsonDeserialize(using = SubRedditDeserializer::class)
data class SubReddit(
    var kind: String = "",
    var name: String = "",
    var subscribers: Int = 0,
    var iconImageUrl: String = "",
    var publicDescription: String = ""
) : Parcelable {
    constructor(parcel: Parcel) : this() {}

    override fun writeToParcel(parcel: Parcel, flags: Int) {}

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<SubReddit> {
        override fun createFromParcel(parcel: Parcel): SubReddit {
            return SubReddit(parcel)
        }

        override fun newArray(size: Int): Array<SubReddit?> {
            return arrayOfNulls(size)
        }
    }

}