package made.taila.reddit.post

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import made.taila.reddit.R
import made.taila.reddit.databinding.FragmentPostBinding


class PostFragment : Fragment() {

    lateinit var fragmentPostBinding: FragmentPostBinding

    companion object {
        const val ARG_KEY_POST_URL = "POST URL"

        @JvmStatic
        fun newInstance(postUrl: String): PostFragment {
            val fragment = PostFragment()
            fragment.arguments = Bundle().apply {
                putString(ARG_KEY_POST_URL, postUrl)
            }
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        fragmentPostBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_post, container, false)
        fragmentPostBinding.webview.settings.javaScriptEnabled = true
        fragmentPostBinding.webview.settings.domStorageEnabled = true
        fragmentPostBinding.webview.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest): Boolean {
                return false
            }
        }
        fragmentPostBinding.webview.loadUrl(arguments?.getString(ARG_KEY_POST_URL))
        return fragmentPostBinding.root
    }

}