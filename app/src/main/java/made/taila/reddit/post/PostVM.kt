package made.taila.reddit.post

import made.taila.reddit.model.Post

class PostVM(val post: Post) {
    fun title() = post.title
    fun thumbnailUrl() = post.thumbnail
    fun subreddit() = "r/${post.subreddit}"
    fun author() = "u/${post.author}"
    fun preview() = post.preview
    fun score() = "${post.score}"
    fun numComments() = "${post.numComments}"
    fun previewWidth() = post.previewWidth
    fun previewHeight() = post.previewHeight
}