package made.taila.reddit.network

import io.reactivex.Single
import made.taila.reddit.model.FeedResponse
import made.taila.reddit.model.SubRedditSearchResponse
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path
import retrofit2.http.Query

interface RedditAPI {

    sealed class Ordering(val value: String) {
        private companion object {
            const val ORDER_HOT = "hot"
            const val ORDER_TOP = "top"
            const val ORDER_CONTROVERSIAL = "controversial"
            const val ORDER_RISING = "rising"
            const val ORDER_NEW = "new"
        }

        object HOT : Ordering(ORDER_HOT)
        object NEW : Ordering(ORDER_NEW)
        object CONTROVERSIAL : Ordering(ORDER_CONTROVERSIAL)
        object RISING : Ordering(ORDER_RISING)
        object TOP : Ordering(ORDER_TOP)
    }

    @GET("/{ordering}/.json")
    @Headers("Content-Type: application/json")
    fun getFeed(@Path("ordering") order: String): Single<FeedResponse>

    @GET("/r/{subreddit}/{ordering}/.json")
    @Headers("Content-Type: application/json")
    fun getSubRedditFeed(@Path("subreddit") subreddit: String, @Path("ordering") order: String): Single<FeedResponse>

    @GET("subreddits/search/.json")
    @Headers("Content-Type: application/json")
    fun getSubReddits(@Query("q") input: String): Single<SubRedditSearchResponse>

}