package made.taila.reddit.network

import android.content.Context
import made.taila.reddit.App
import made.taila.reddit.ConnectionUtil
import made.taila.reddit.R
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

class ConnectivityInterceptor : Interceptor {
    @Inject
    lateinit var connectionUtil: ConnectionUtil

    @Inject
    lateinit var context: Context

    init {
        App.component.inject(this)
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        if (!connectionUtil.hasConnection()) throw NoConnectionError(context.getString(R.string.no_internent_connection))
        return chain.proceed(chain.request())
    }

    class NoConnectionError(message: String) : Throwable(message)
}