package made.taila.reddit

import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import made.taila.reddit.di.AppComponent
import made.taila.reddit.di.DaggerAppComponent

class App : DaggerApplication() {

    companion object {
        lateinit var component: AppComponent
        const val TAG = "TM"
    }

    override fun applicationInjector(): AndroidInjector<out App> {
        component = DaggerAppComponent.builder().create(this) as AppComponent
        component.inject(this)
        return component
    }

}