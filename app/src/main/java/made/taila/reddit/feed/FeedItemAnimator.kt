package made.taila.reddit.feed

import android.animation.AnimatorSet
import android.support.v4.content.ContextCompat
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.RecyclerView
import android.widget.ImageView
import made.taila.reddit.R
import made.taila.reddit.ui.popAnimation
import made.taila.reddit.ui.tintAnimator

class FeedItemAnimator : DefaultItemAnimator() {

    companion object : ItemHolderInfo() {
        const val ACTION_UPVOTED = 0
        const val ACTION_DOWNVOTED = 1
        var updateAction: HashMap<Int, Int> = hashMapOf()
    }

    override fun canReuseUpdatedViewHolder(viewHolder: RecyclerView.ViewHolder): Boolean {
        return true
    }

    override fun recordPreLayoutInformation(
        state: RecyclerView.State,
        viewHolder: RecyclerView.ViewHolder,
        changeFlags: Int, payloads: List<Any>
    ): RecyclerView.ItemAnimator.ItemHolderInfo {
        if (changeFlags == RecyclerView.ItemAnimator.FLAG_CHANGED) {
            for (payload in payloads) {
                if (payload is HashMap<*, *>) {
                    FeedItemAnimator.updateAction = payload as HashMap<Int, Int>
                    return FeedItemAnimator.Companion
                }
            }
        }

        return super.recordPreLayoutInformation(state, viewHolder, changeFlags, payloads)
    }

    override fun animateChange(
        oldHolder: RecyclerView.ViewHolder,
        newHolder: RecyclerView.ViewHolder,
        preInfo: ItemHolderInfo,
        postInfo: ItemHolderInfo
    ): Boolean {
        if (preInfo is FeedItemAnimator.Companion) {
            newHolder as FeedAdapter.PostVH
            if (preInfo.updateAction.containsKey(ACTION_UPVOTED)) { // Expand
                if (newHolder.adapterPosition == preInfo.updateAction[ACTION_UPVOTED]) {
                    upvote(newHolder)
                }
            } else {
                if (newHolder.adapterPosition == preInfo.updateAction[ACTION_DOWNVOTED]) {
                    downvote(newHolder)
                }
            }
        }

        return false
    }

    fun animateVoteBtn(imageView: ImageView, colorTo: Int) {
        AnimatorSet().apply {
            playTogether(
                imageView.popAnimation(1.8f),
                imageView.tintAnimator(intArrayOf(colorTo))
            )
        }.start()
    }

    fun upvote(holder: FeedAdapter.PostVH) {
        animateVoteBtn(holder.itemPostBinding.ivUpvote, colorTo = ContextCompat.getColor(holder.itemView.context, R.color.orange))
        holder.itemPostBinding.ivDownvote.tintAnimator(intArrayOf(ContextCompat.getColor(holder.itemView.context, android.R.color.black))).start()
    }

    fun downvote(holder: FeedAdapter.PostVH) {
        animateVoteBtn(holder.itemPostBinding.ivDownvote, colorTo = ContextCompat.getColor(holder.itemView.context, R.color.orange))
        holder.itemPostBinding.ivUpvote.tintAnimator(intArrayOf(ContextCompat.getColor(holder.itemView.context, android.R.color.black))).start()
    }

}