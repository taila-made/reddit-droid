package made.taila.reddit.feed

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.res.ColorStateList
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.core.view.children
import made.taila.reddit.MainVM
import made.taila.reddit.R
import made.taila.reddit.databinding.FragmentFeedBinding
import made.taila.reddit.model.SubReddit
import made.taila.reddit.network.RedditAPI
import made.taila.reddit.ui.onSelectAnimator

class FeedFragment : Fragment() {

    lateinit var feedBinding: FragmentFeedBinding
    lateinit var feedVM: FeedVM
    lateinit var mainVM: MainVM

    companion object {
        const val ARG_KEY_SUBREDDIT = "SUBREDDIT"

        @JvmStatic
        fun newInstance(subReddit: SubReddit?): FeedFragment {
            val fragment = FeedFragment()
            fragment.arguments = Bundle().apply {
                putParcelable(ARG_KEY_SUBREDDIT, subReddit)
            }
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        feedVM = ViewModelProviders.of(this)[FeedVM::class.java]
        mainVM = ViewModelProviders.of(activity!!)[MainVM::class.java]
        feedVM.subReddit = arguments?.getParcelable(ARG_KEY_SUBREDDIT)
        lifecycle.addObserver(feedVM)
        observe()
    }

    private fun observe() {
        feedVM.ordering.observe(this, Observer { it ->
            val btnToAnimate = when (it) {
                is RedditAPI.Ordering.HOT -> feedBinding.btnHot
                is RedditAPI.Ordering.NEW -> feedBinding.btnNew
                is RedditAPI.Ordering.CONTROVERSIAL -> feedBinding.btnControversial
                is RedditAPI.Ordering.RISING -> feedBinding.btnRising
                is RedditAPI.Ordering.TOP -> feedBinding.btnTop
                else -> feedBinding.btnHot
            }
            animateSelectOrderBtn(btnToAnimate)
        })
    }

    private fun setOnClickListeners() {
        feedBinding.btnOrderingWrapper.children.forEach { view ->
            view as Button
            view.setOnClickListener {
                if (!it.isSelected) {
                    selectOrderBtn(view)
                    deselectOtherBtns(view)
                }
            }
        }
    }

    private fun deselectOtherBtns(btn: Button) {
        feedBinding.btnOrderingWrapper.children
                .filter { it -> btn.id != it.id }
                .forEach { it ->
                    if (it.isSelected) {
                        animateDeselectOrderBtn(it as Button)
                    }
                    it.isSelected = false
                }
    }

    private fun selectOrderBtn(btn: Button) {
        btn.isSelected = true
        feedVM.ordering.value = when (btn.id) {
            feedBinding.btnHot.id -> RedditAPI.Ordering.HOT
            feedBinding.btnNew.id -> RedditAPI.Ordering.NEW
            feedBinding.btnControversial.id -> RedditAPI.Ordering.CONTROVERSIAL
            feedBinding.btnRising.id -> RedditAPI.Ordering.RISING
            feedBinding.btnTop.id -> RedditAPI.Ordering.TOP
            else -> RedditAPI.Ordering.HOT
        }
    }

    private fun animateSelectOrderBtn(chip: Button) {
        chip.onSelectAnimator(toColor = ContextCompat.getColor(context!!, R.color.blue)).start()
        chip.setTextAppearance(R.style.RedditTextAppearance_Btn_White)
    }

    private fun animateDeselectOrderBtn(chip: Button) {
        chip.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(context!!, android.R.color.white))
        chip.setTextAppearance(R.style.RedditTextAppearance_Btn)
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        feedBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_feed, container, false)
        feedBinding.btnHot.isSelected = true
        setupRecyclerView()
        setOnClickListeners()
        return feedBinding.root
    }

    private fun setupRecyclerView() {
        feedBinding.feedRecyclerView.adapter = FeedAdapter(feedVM, mainVM, this)
        feedBinding.feedRecyclerView.layoutManager = LinearLayoutManager(context)
        feedBinding.feedRecyclerView.itemAnimator = FeedItemAnimator()
    }
}