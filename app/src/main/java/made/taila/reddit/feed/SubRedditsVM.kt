package made.taila.reddit.feed

import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.util.Log
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import made.taila.reddit.App
import made.taila.reddit.model.SubRedditSearchResponse
import made.taila.reddit.network.RedditAPI
import javax.inject.Inject

class SubRedditsVM : ViewModel(), LifecycleObserver {

    private val compositeDisposable = CompositeDisposable()

    @Inject
    lateinit var redditAPI: RedditAPI

    val subRedditsSearchResponse = MutableLiveData<SubRedditSearchResponse>()

    init {
        App.component.inject(this)
    }

    fun searchForSubReddits(searchTerm: String) {
        compositeDisposable.add(
            redditAPI.getSubReddits(searchTerm)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    subRedditsSearchResponse.value = it
                }, {
                    Log.i(App.TAG, it.localizedMessage)
                })
        )
    }

    override fun onCleared() {
        compositeDisposable.clear()
        super.onCleared()
    }

}