package made.taila.reddit.feed

import android.arch.lifecycle.Observer
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import made.taila.reddit.MainVM
import made.taila.reddit.R
import made.taila.reddit.common.Event
import made.taila.reddit.databinding.ItemPostBinding
import made.taila.reddit.model.Post
import made.taila.reddit.post.PostVM
import made.taila.reddit.ui.getScreenWidth
import made.taila.reddit.ui.onClick

class FeedAdapter(private val feedVM: FeedVM, private val mainVM: MainVM, fragment: FeedFragment) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    init {
        feedVM.feedResponse.observe(fragment, Observer {
            notifyDataSetChanged()
        })
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = DataBindingUtil.inflate<ItemPostBinding>(
                LayoutInflater.from(parent.context),
                R.layout.item_post,
                parent,
                false
        )
        return PostVH(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        holder as PostVH
        holder.bind(position)
    }

    private fun getPostAtPosition(position: Int): Post {
        return feedVM.feedResponse.value!!.posts[position]
    }

    override fun getItemCount(): Int {
        return try {
            feedVM.feedResponse.value!!.posts.size
        } catch (e: NullPointerException) {
            0
        }
    }

    inner class PostVH(val itemPostBinding: ItemPostBinding) : RecyclerView.ViewHolder(itemPostBinding.root) {
        fun bind(position: Int) {
            val post = getPostAtPosition(position)
            itemPostBinding.postVM = PostVM(post)
            scaleImageDimens(post)
            itemPostBinding.notifyChange()

            itemPostBinding.groupMainContent.onClick(View.OnClickListener {
                mainVM.postSelected.value = Event(post)
            })
            itemPostBinding.ivUpvote.setOnClickListener {
                val payload = hashMapOf(Pair(FeedItemAnimator.ACTION_UPVOTED, position))
                notifyItemChanged(position, payload)
            }
            itemPostBinding.ivDownvote.setOnClickListener {
                val payload = hashMapOf(Pair(FeedItemAnimator.ACTION_DOWNVOTED, position))
                notifyItemChanged(position, payload)
            }
        }

        private fun scaleImageDimens(post: Post) {
            val horizontalMargin = itemPostBinding.root.context.resources.getDimensionPixelOffset(R.dimen.margin_s) * 4
            val viewWidth = getScreenWidth() - horizontalMargin
            if (post.previewWidth > viewWidth) {
                val scaleDownPercent = viewWidth / post.previewWidth.toFloat()
                post.previewWidth = (scaleDownPercent * post.previewWidth).toInt()
                post.previewHeight = (scaleDownPercent * post.previewHeight).toInt()
            }
        }
    }
}