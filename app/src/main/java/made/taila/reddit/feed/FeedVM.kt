package made.taila.reddit.feed

import android.arch.lifecycle.*
import android.util.Log
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import made.taila.reddit.App
import made.taila.reddit.model.FeedResponse
import made.taila.reddit.model.SubReddit
import made.taila.reddit.network.RedditAPI
import javax.inject.Inject

class FeedVM : ViewModel(), LifecycleObserver {

    private var firstStart = true
    private val compositeDisposable = CompositeDisposable()
    var subReddit: SubReddit? = null
    var ordering = MutableLiveData<RedditAPI.Ordering>()

    @Inject
    lateinit var redditAPI: RedditAPI

    /**
     *  TODO:
     *  Each feed response only gives us X posts (e.g. 25 per page) This is a list with the hopes to add pagination
     *  Therefore instead of a single feed response we may need a list of feed responses or something similar, each one holding a page worth of posts
     */
    val feedResponse = MutableLiveData<FeedResponse>()

    init {
        App.component.inject(this)
        feedResponse.value = FeedResponse()
        ordering.value = RedditAPI.Ordering.HOT
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun onFragmentCreate() {
        if (firstStart) {
            fetchFeed()
            firstStart = false
        }
    }

    private fun fetchFeed(order: String = RedditAPI.Ordering.HOT.value) {
        compositeDisposable.add(
                if (subReddit?.name.isNullOrEmpty()) {
                    redditAPI.getFeed(order)
                } else {
                    redditAPI.getSubRedditFeed(subReddit?.name!!, order)
                }.subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({
                            feedResponse.value = it
                        }, {
                            Log.i(App.TAG, it.localizedMessage)
                        })
        )
    }

    override fun onCleared() {
        compositeDisposable.dispose()
        super.onCleared()
    }


}