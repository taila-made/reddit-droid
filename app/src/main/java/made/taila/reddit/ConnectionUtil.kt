package made.taila.reddit

import android.net.ConnectivityManager
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ConnectionUtil @Inject constructor(private var connectivityManager: ConnectivityManager) {
    fun hasConnection() = connectivityManager.activeNetworkInfo != null && connectivityManager.activeNetworkInfo.isConnected
}