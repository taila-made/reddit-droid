package made.taila.reddit.di

import android.content.Context
import android.net.ConnectivityManager
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import dagger.Module
import dagger.Provides
import dagger.Reusable
import made.taila.reddit.App
import made.taila.reddit.BuildConfig
import made.taila.reddit.network.ConnectivityInterceptor
import made.taila.reddit.network.RedditAPI
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.jackson.JacksonConverterFactory

@Module
object NetworkModule {

    private const val BASE_URL = "https://www.reddit.com/"

    @Provides
    @Reusable
    @JvmStatic
    internal fun provideConnectivityManager(app: App): ConnectivityManager {
        return app.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    }

    @Provides
    @Reusable
    @JvmStatic
    internal fun provideOkHttpClient(): OkHttpClient {
        val okHttpClient = OkHttpClient.Builder()
        if (BuildConfig.DEBUG) {
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY

            okHttpClient.addInterceptor(logging)
        }
        okHttpClient.addInterceptor(ConnectivityInterceptor())

        return okHttpClient.build()
    }

    @Provides
    @Reusable
    @JvmStatic
    internal fun provideRetrofitInterface(okHttpClient: OkHttpClient): Retrofit {
        val objectMapper = ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)

        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(JacksonConverterFactory.create(objectMapper))
            .client(okHttpClient)
            .build()
    }

    @Provides
    @Reusable
    @JvmStatic
    internal fun provideRedditAPI(retrofit: Retrofit): RedditAPI {
        return retrofit.create(RedditAPI::class.java)
    }

}