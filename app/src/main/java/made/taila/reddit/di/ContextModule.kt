package made.taila.reddit.di

import android.content.Context
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import made.taila.reddit.App
import made.taila.reddit.MainActivity

/**
 * Module which provides all required dependencies about Context
 */
@Module
@Suppress("unused")
abstract class ContextModule {

    @ContributesAndroidInjector(modules = [ActivityModule::class])
    abstract fun main(): MainActivity

    @Binds
    abstract fun bindApp(app: App): Context
}