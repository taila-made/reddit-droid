package made.taila.reddit.di

import android.content.Context
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import made.taila.reddit.App
import made.taila.reddit.MainActivity
import made.taila.reddit.feed.FeedVM
import made.taila.reddit.feed.SubRedditsVM
import made.taila.reddit.network.ConnectivityInterceptor
import javax.inject.Singleton

@Singleton
@Component(modules = [ContextModule::class, NetworkModule::class, AndroidSupportInjectionModule::class])
interface AppComponent : AndroidInjector<App> {
    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<App>()

    fun app(): App
    fun context(): Context

    fun inject(activity: MainActivity)
    fun inject(connectivityInterceptor: ConnectivityInterceptor)
    fun inject(vm: FeedVM)
    fun inject(vm: SubRedditsVM)
}